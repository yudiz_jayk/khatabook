const mongoose = require('mongoose')
const config = require('../config')

const connetion = mongoose.connect(
  config.DB_URI,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  },
  (err) => {
    if (err)console.log(err)
    else console.log('Connected to DB')
  }

)

module.exports = connetion
