const utilities = {}
const config = require('../config')
const client = require('twilio')(config.ACCOUNTSID, config.AUTHTOKEN)
const User = require('../graphql/User/model')
const ObjectId = require('mongoose').Types.ObjectId
const Entry = require('../graphql/Entries/model')
const Customer = require('../graphql/Customer/model')

utilities.checkName = async (sName) => {
  return /^[a-zA-Z ]*$/.test(sName)
}

utilities.checkUniqueMobile = async (sMobileNo) => {
  try {
    const user = await User.findOne({ sMobileNo, eStatus: 'Y', isMobileVerified: true })
    if (user) return false
    else return true
  } catch (error) {
    console.log(error)
  }
}

utilities.sendSMS = async (sMobileNo, nOtp) => {
  try {
    const response = await client.messages
      .create({
        body: nOtp,
        from: '+12566459412',
        to: `+${sMobileNo}`
      })

    return response.sid
  } catch (error) {
    console.log(error)
  }
}

utilities.genrateOtp = () => {
  const val = Math.floor(1000 + Math.random() * 9000)
  return val
}

utilities.calculateNetAmount = async (iUserId, iCustomerId) => {
  try {
    const results = await Entry.aggregate([
      {
        $facet:
            {
              paidByCustomer: [
                {
                  $match: {
                    iUserId: ObjectId(iUserId),
                    iCustomerId: ObjectId(iCustomerId),
                    paidByUser: false,
                    eStatus: 'Y'
                  }
                },
                {
                  $group: {
                    _id: '_id',
                    totalAmount: { $sum: '$nAmount' }
                  }
                }
              ],
              paidByUser: [
                {
                  $match: {
                    iUserId: ObjectId(iUserId),
                    iCustomerId: ObjectId(iCustomerId),
                    paidByUser: true,
                    eStatus: 'Y'
                  }
                },
                {
                  $group: {
                    _id: '_id',
                    totalAmount: { $sum: '$nAmount' }
                  }
                }
              ]
            }

      }

    ])

    const paidByCustomer = results[0].paidByCustomer[0] ? results[0].paidByCustomer[0].totalAmount : 0
    const paidByUser = results[0].paidByUser[0] ? results[0].paidByUser[0].totalAmount : 0

    const nNetAmount = paidByUser - paidByCustomer
    return nNetAmount
  } catch (error) {
    console.log(error)
  }
}

utilities.calcKhatabookamount = async (iUserId, iKhatabookId) => {
  try {
    const customerAmount = await Customer.aggregate([
      {
        $match: {
          iUserId,
          iKhatabookId: ObjectId(iKhatabookId),
          eStatus: 'Y'
        }

      },
      {
        $group: {
          _id: '$iKhatabookId',
          nYouwiilGet: {
            $sum: {
              $cond: [{ $lt: ['$nAmount', 0] }, '$nAmount', 0]
            }
          },
          nYouwilGive: {
            $sum: {
              $cond: [{ $gt: ['$nAmount', 0] }, '$nAmount', 0]
            }
          }

        }
      },
      {
        $project: { _id: 0 }
      }
    ])

    return customerAmount?.[0]
  } catch (error) {
    console.log(error)
  }
}

module.exports = utilities
