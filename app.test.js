/* eslint-disable */
const mongoose = require('mongoose')
let connetion

const request = require('supertest')
const server = require('./server')

const queryData = {
  query: `query GetKhatabookById($getKhatabookByIdId: ID!) {
          getKhatabookById(id: $getKhatabookByIdId) {
                sName
                sBusinessName
                sBusinessDetail
            }
        }`,
  variables: { getKhatabookByIdId: '6305e8a894c9fedf75f59566' }
}

describe('KhataBookTesting', () => {
  beforeAll(async () => {
    connetion = await mongoose.connect('mongodb+srv://emall:emall@cluster0.fynsp.mongodb.net/Khatabook?retryWrites=true&w=majority')
  })

  afterAll(async () => {
    (await server).server.close()
    connetion.disconnect()
  })

  it('test GetKhataBookById', async () => {
    const response = await request((await server).url)
      .post('/')
      .set('authorization', 'utoken eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmZjODY2YzgyOWExZmYyYjhlMjQxZmIiLCJpYXQiOjE2NjIwMjYwOTcsImV4cCI6MTY2MjExMjQ5N30.KWy6Hazz-eKwZ9QuYJ2pGdKlcqifY1iLLru3GBAkgPo')
      .send(queryData)

    const data = response.body.data.getKhatabookById
    expect(data.sName).not.toBe(undefined)
    expect(data.sName).not.toBe(null)

    expect(data.sBusinessName).not.toBe(undefined)
    expect(data.sBusinessName).not.toBe(null)

    expect(data.sBusinessDetail).not.toBe(undefined)
    expect(data.sBusinessDetail).not.toBe(null)
  })
})
