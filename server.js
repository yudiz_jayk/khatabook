const express = require('express')
const app = express()
const { ApolloServer } = require('apollo-server')
const { ApolloServerPluginLandingPageLocalDefault } = require('apollo-server-core')
const schema = require('./graphql/index')
const { context } = require('./graphql/contex')
require('./db/index')
require('dotenv').config()
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
const PORT = process.env.PORT || 4000

const server = new ApolloServer({
  schema,
  csrfPrevention: true,
  cache: 'bounded',
  context,
  introspection: true,
  plugins: [ApolloServerPluginLandingPageLocalDefault({ embed: true })]
})

const serverInfo = server.listen(PORT)

if (process.env.NODE_ENV !== 'test') {
  console.log('Listing on port ', PORT)
}

module.exports = serverInfo
