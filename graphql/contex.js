
const User = require('./User/model')
const Admin = require('./Admin/model')
const { InternalServerError } = require('./customError')
const adminDataLoaders = require('./Admin/dataLoaders')

module.exports.context = async ({ req }) => {
  let sToken = req.header('authorization')
  if (sToken) sToken = sToken.split(' ')

  try {
    if (sToken) {
      let user, admin

      if (sToken[0] === 'utoken') {
        user = await User.findByToken(sToken[1])
      } else if (sToken[0] === 'atoken') {
        admin = await Admin.findByToken(sToken[1])
      }

      if (!user && !admin) {
        return null
      }

      if (user) return { user }

      if (admin) {
        return {
          admin,
          loaders: {
            khatabookbyUid: adminDataLoaders.khatabookByuserid(),
            customersbyKid: adminDataLoaders.customerBykhatabookid()
          }
        }
      }
    } else {
      return null
    }
  } catch (error) {
    if (error.message === 'jwt expired') {
      return { error: 'Session expired Please loggedIn ' }
    }
    throw new InternalServerError('Error', { error: 'Something went wrong' })
  }
}
