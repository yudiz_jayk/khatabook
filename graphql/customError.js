const { ApolloError } = require('apollo-server-errors')

module.exports.GenralError = class GenralError extends ApolloError {
  constructor (message, extensions) {
    super(message, 'GENRAL_ERROR', extensions)
    Object.defineProperty(this, 'name', { value: 'Genral_Error' })
  }
}

module.exports.InternalServerError = class GenralError extends ApolloError {
  constructor (message, extensions) {
    super(message, 'INTERNAL_SERVER_ERROR', extensions)
    Object.defineProperty(this, 'name', { value: 'Internal_Server_Error' })
  }
}
