const { AuthenticationError } = require('apollo-server-core')
const { rule } = require('graphql-shield')
const { InternalServerError } = require('./customError')

const rules = {}

rules.isUserauthenticated = rule({ cache: 'contextual' })(
  async (parent, args, ctx, info) => {
    try {
      if (ctx.error) return new AuthenticationError('Error', { error: ctx.error })
      if (!ctx.user) return new AuthenticationError('Error', { error: 'Unauthenticated Access denied' })
      return true
    } catch (error) {
      throw new InternalServerError('Error', { error: 'Something went wrong' })
    }
  }
)

rules.isAdminauthenticated = rule({ cache: 'contextual' })(
  async (parent, args, ctx, info) => {
    try {
      if (ctx.error) return new AuthenticationError('Error', { error: ctx.error })
      if (!ctx.admin) return new AuthenticationError('Error', { error: 'Unauthorized access denied' })
      return true
    } catch (error) {
      throw new InternalServerError('Error', { error: 'Something went wrong' })
    }
  }
)

module.exports = rules
