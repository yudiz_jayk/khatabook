const Khatabook = require('./model')
const { validateKhataBook } = require('./validator')
const { UserInputError } = require('apollo-server-core')
const { GenralError, InternalServerError } = require('../customError')
const ObjectId = require('mongoose').Types.ObjectId
const Customer = require('../Customer/model')
const { calcKhatabookamount } = require('../../utils/utilites')

const resolvers = {

  Query: {
    getKhatabook: async (parent, args, context, info) => {
      try {
        const getkhatabooks = await Khatabook.find({ iUserId: context.user._id, eStatus: 'Y' })
        return getkhatabooks
      } catch (error) {
        throw new InternalServerError('Error', { error: 'Something went wrong' })
      }
    },
    getKhatabookById: async (parent, args, context, info) => {
      try {
        if (!(args.id.trim().length > 0 && ObjectId.isValid(args.id))) return new GenralError('Error', { error: 'Invalid ObjectID' })
        const getkhatabooks = await Khatabook.findById(args.id).where('eStatus').equals('Y')
        return getkhatabooks
      } catch (error) {
        throw new InternalServerError('Error', { error: 'Something went wrong' })
      }
    },
    khatabookWithdetail: async (parent, args, context, info) => {
      const { id } = args

      const khatabook = await Khatabook.findById(id).lean()
      const amountDetail = await calcKhatabookamount(context.user._id, id)
      return { ...khatabook, ...amountDetail }
    }
  },
  khataBookwithdetail: {
    aCustomers: async (parent, args, context, info) => {
      try {
        const customers = await Customer.find(parent._id).lean()
        return customers
      } catch (error) {
        throw new InternalServerError('Error', { error: 'Something went wrong' })
      }
    }
  },
  Mutation: {
    addKhatabook: async (parent, args, context, info) => {
      try {
        const { sName = undefined, sBusinessName, sBusinessDetail } = args.khatabook
        const errors = await validateKhataBook(sName, sBusinessName, sBusinessDetail)

        if (!Object.keys(errors).length < 1) return new UserInputError('Errors', { errors })

        const addbook = await Khatabook.create({ ...args.khatabook, iUserId: context.user._id })
        return addbook
      } catch (error) {
        throw new InternalServerError('Error', { error: 'Something went wrong' })
      }
    },
    updateKhatabook: async (parent, args, context, info) => {
      try {
        if (args.id < 1 && args.id === '') return new UserInputError('Error', { error: 'invalid id' })

        const { sName = undefined, sBusinessName, sBusinessDetail } = args.khatabookdetail

        const errors = await validateKhataBook(sBusinessName, sBusinessDetail)

        if (!Object.keys(errors).length < 1) return new UserInputError('Errors', { errors })

        const updatedkhatabook = await Khatabook.findByIdAndUpdate(args.id, { sName, sBusinessName, sBusinessDetail }, { new: true })

        if (!updatedkhatabook) return new GenralError('Error', { error: 'Khatabook Not Found' })
        return { sMessage: 'Khatabook updated successfully' }
      } catch (error) {
        throw new InternalServerError('Error', { error: 'Something went wrong' })
      }
    },
    deleteKhatabook: async (parent, args, context, info) => {
      try {
        const deleteKhataBook = await Khatabook.findByIdAndUpdate({ _id: args.id, iUserId: context.user._id }, { eStatus: 'D' })
        if (!deleteKhataBook) return new GenralError('Error', { error: 'Khatabook Not Found' })
        return { sMessage: `${args.id} khatabook deleted successfully` }
      } catch (error) {
        throw new InternalServerError('Error', { error: 'Something went wrong' })
      }
    }
  }
}

module.exports = resolvers
