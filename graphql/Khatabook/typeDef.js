const typeDefs = `
    type Khatabook {
      sName: String,
      sBusinessName: String,
      sBusinessDetail: String
    }
    
    type responseMsgbook{
      sMessage:String
    }

    type customer{
      _id:String
      sCustomerName:String,
      nAmount:Int
    }

    type khataBookwithdetail{
      sName: String,
      nYouwiilGet:Int
      nYouwilGive:Int
      aCustomers:[customer]
    }

    input createKhatabook {
      sName: String,
      sBusinessName: String!,
      sBusinessDetail: String!
    }

    type Query {
      getKhatabook: [Khatabook],
      getKhatabookById(id:ID!):Khatabook,
      khatabookWithdetail(id:ID):khataBookwithdetail
    }
    
    type Mutation {
      addKhatabook(khatabook: createKhatabook): Khatabook!
      updateKhatabook(id:ID!, khatabookdetail: createKhatabook ): responseMsgbook!
      deleteKhatabook(id:ID!): responseMsgbook!
    } 
    `

module.exports = typeDefs
