const mongoose = require('mongoose')

const katabookSchema = new mongoose.Schema({
  sName: String,
  sBusinessName: String,
  sBusinessDetail: String,
  iUserId: {
    type: mongoose.Types.ObjectId,
    ref: 'User'
  },
  eStatus: {
    type: String,
    default: 'Y',
    enum: ['Y', 'D']

  }
}, {
  timestamps: true
})

module.exports = mongoose.model('Khatabook', katabookSchema)
