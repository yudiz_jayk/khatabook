const validator = {}

validator.validateKhataBook = async (sName, sBusinessName, sBusinessDetail) => {
  const errors = {}
  if (sName) {
    if (!(sName.trim() && /^[a-zA-Z ]*$/.test(sName))) {
      errors.sBusinessName = 'Only alphabets allowed'
    } else if (!(sBusinessName.length >= 4)) {
      errors.sBusinessName = 'BusinessName should be greaterthan 4 characters'
    }
  }

  if (!(sBusinessName && sBusinessName.trim() && /^[a-zA-Z ]*$/.test(sBusinessName))) {
    errors.sBusinessName = 'Only alphabets allowed'
  } else if (!(sBusinessName.length >= 4)) {
    errors.sBusinessName = 'BusinessName should be greaterthan 4 characters'
  }

  if (!(sBusinessDetail)) {
    errors.sBusinessDetail = 'Business Detail required'
  } else if (!(sBusinessDetail.length >= 4)) {
    errors.sBusinessDetail = 'BusinessDetail should be greaterthan 4 characters'
  }
  return errors
}

module.exports = validator
