const { makeExecutableSchema } = require('@graphql-tools/schema')
const { merge } = require('lodash')
const { applyMiddleware } = require('graphql-middleware')
const permissions = require('./Permissions')
const khatabookRes = require('./Khatabook/resolver')
const khatabookTdef = require('./Khatabook/typeDef')
const userTdefs = require('./User/typeDef')
const userRes = require('./User/resolver')
const customerTdefs = require('./Customer/typeDef')
const customerRes = require('./Customer/resolver')
const entryTdef = require('./Entries/typeDef')
const entryRes = require('./Entries/resolver')
const adminTdef = require('./Admin/typeDef')
const adminRes = require('./Admin/resolver')

let schema = makeExecutableSchema({
  typeDefs: [userTdefs, khatabookTdef, customerTdefs, entryTdef, adminTdef],
  resolvers: merge(userRes, khatabookRes, customerRes, entryRes, adminRes)
})

schema = applyMiddleware(schema, permissions)

module.exports = schema
