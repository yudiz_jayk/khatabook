const { shield, allow } = require('graphql-shield')
const rules = require('./rules')
const permissions = shield({
  Query: {
    getKhatabook: rules.isUserauthenticated,
    getEntries: rules.isUserauthenticated,
    getEntryById: rules.isUserauthenticated,
    getKhatabookById: rules.isUserauthenticated,
    khatabookWithdetail: rules.isUserauthenticated,
    getCustomer: rules.isUserauthenticated,
    getDetails: rules.isAdminauthenticated
  },
  Mutation: {
    addKhatabook: rules.isUserauthenticated,
    updateKhatabook: rules.isUserauthenticated,
    deleteKhatabook: rules.isUserauthenticated,
    addCustomer: rules.isUserauthenticated,
    updateCustomer: rules.isUserauthenticated,
    deleteCustomer: rules.isUserauthenticated,
    createEntry: rules.isUserauthenticated,
    updateEntry: rules.isUserauthenticated,
    deleteEntry: rules.isUserauthenticated
  }
}, {
  allowExternalErrors: allow
})

module.exports = permissions
