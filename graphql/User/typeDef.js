const typeDefs = `
    type user{
      sMessage:String
    }

    type responseMsg{
      sMessage:String
      sToken:String
    }
  
    type Query{
       getUser : user
    }

    input inputUser{
      sName:String!
      sMobileNo:String!
    }

    input inputVerifyMobile{
       sMobileNo:String!
       sOtp:String!
    }

    input loginInput{
       sMobileNo:String!
    }

    input verifyLogin{
      sMobileNo:String!
      sOtp:String!
    }

    type Mutation{
      register(user:inputUser):responseMsg!
      verifyMobile(verifymobile:inputVerifyMobile):responseMsg!
      login(login:loginInput):responseMsg!
      verifyLogin(verifylogin:verifyLogin):responseMsg!
    }
`

module.exports = typeDefs
