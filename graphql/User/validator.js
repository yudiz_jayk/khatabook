
const validator = {}

validator.validateRegister = async (sName, sMobileNo) => {
  const errors = {}
  if (!(sName && sName.trim() && /^[a-zA-Z ]*$/.test(sName))) {
    errors.sName = 'Only alphabets allowed'
  } else if (!(sName.length >= 4)) {
    errors.sName = 'Name should be gretarthen 4 characters'
  }

  if (!/(0|91)[7-9][0-9]{9}/.test(sMobileNo)) {
    errors.sMobileNo = 'Invalid mobileNo formate'
  }

  return errors
}

validator.validateVerifyMobile = async (sMobileNo, sOtp) => {
  const errors = {}
  if (!/(0|91)[7-9][0-9]{9}/.test(sMobileNo)) {
    errors.sMobileNo = 'Invalid mobileNo formate'
  }

  if (!sOtp && !sOtp.trim() >= 4) {
    errors.nOtp = 'Please enter Verification Code'
  }

  return errors
}

validator.validateLogin = async (sMobileNo) => {
  const errors = {}
  if (!/(0|91)[7-9][0-9]{9}/.test(sMobileNo)) {
    errors.sMobileNo = 'Invalid mobileNo formate'
  }
  return errors
}

validator.validateVerifyLogin = async (sMobileNo, sOtp) => {
  console.log(sOtp)
  const errors = {}
  if (!/(0|91)[7-9][0-9]{9}/.test(sMobileNo)) {
    errors.sMobileNo = 'Invalid mobileNo formate'
  }

  if (!(sOtp && sOtp.trim().length >= 4)) {
    errors.sOtp = 'Please Enter full otp'
  }
  return errors
}

module.exports = validator
