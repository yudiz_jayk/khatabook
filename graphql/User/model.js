const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')
const config = require('../../config')

const userSchema = new mongoose.Schema({
  sName: String,
  sMobileNo: String,

  isMobileVerified: {
    type: Boolean,
    default: false
  },

  eStatus: {
    type: String,
    enum: ['Y', 'N', 'D'],
    default: 'Y'
  },

  sLoginOtp: String,
  sMobileOtp: String,
  nMobileOtpExpiry: Number,
  nLoginOtpExpiry: Number,

  aTokens: [{
    sToken: String
  }]

}, {
  timestamps: true
})

userSchema.statics.findByToken = function (token) {
  const user = this
  let decoded
  try {
    decoded = jwt.verify(token, config.JWT_SECRET)
  } catch (e) {
    return Promise.reject(e)
  }
  const query = {
    _id: decoded._id,
    'aToken.sToken': token,
    eStatus: 'Y'
  }
  return user.findOne(query)
}

module.exports = mongoose.model('User', userSchema)
