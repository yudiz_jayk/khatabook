
const { sendSMS, genrateOtp } = require('../../utils/utilites')
const User = require('./model')
const { validateRegister, validateVerifyMobile, validateLogin, validateVerifyLogin } = require('./validator')
const { UserInputError, AuthenticationError } = require('apollo-server-core')
const config = require('../../config')
const jwt = require('jsonwebtoken')
const { GenralError, InternalServerError } = require('../customError')

const resolvers = {
  Mutation: {
    register: async (parent, args, context, info) => {
      try {
        const { sMobileNo, sName } = args.user
        const errors = await validateRegister(sName, sMobileNo)
        const nOtp = genrateOtp()

        if (!Object.keys(errors).length < 1) return new UserInputError('Errors', { errors })

        const user = await User.findOne({ sMobileNo })

        if (user && user.isMobileVerified === false) {
          if (user.eStatus === 'D') await User.findByIdAndUpdate(user._id, { eStatus: 'Y', ...args.user })
          await sendSMS(sMobileNo, nOtp)
          await User.findByIdAndUpdate(user._id, { sMobileOtp: nOtp.toString(), nMobileOtpExpiry: Date.now() + 300000 })
          return { sMessage: 'Please verify your mobileNo By verification code' }
        } else if (user && user.isMobileVerified === true) {
          errors.sMobileNo = 'Mobile Number should be unique'
          return new UserInputError('Errors', { errors })
        }

        await User.create({ ...args.user, sMobileOtp: nOtp.toString(), nMobileOtpExpiry: Date.now() + 300000 })
        await sendSMS(sMobileNo, nOtp)

        return { sMessage: 'Please verify your mobileNo By verification code' }
      } catch (error) {
        throw new InternalServerError('Error', { error: 'Something went wrong' })
      }
    },

    verifyMobile: async (parent, args, context, info) => {
      try {
        const { sMobileNo, sOtp } = args.verifymobile
        const errors = await validateVerifyMobile(sMobileNo, sOtp)

        if (!Object.keys(errors).length < 1) return new UserInputError('Errors', { errors })

        const user = await User.findOne({ sMobileNo, isMobileVerified: false })

        if (!user) {
          return new UserInputError('Error', { error: 'User with this MobileNo dose not exists' })
        }

        if (!(user.nMobileOtpExpiry > Date.now())) throw new GenralError('Error', { error: 'Otp Expired please sent again' })

        if (user.sMobileOtp === sOtp) {
          await User.findByIdAndUpdate(user._id, { isMobileVerified: true, $unset: { sMobileOtp: '', nMobileOtpExpiry: '' } })

          return { sMessage: 'Registration successfully' }
        } else {
          return new UserInputError('Error', { error: 'Invalid Verification Code' })
        }
      } catch (error) {
        throw new InternalServerError('Error', { error: 'Something went wrong' })
      }
    },

    login: async (parent, args, context, info) => {
      try {
        const { sMobileNo } = args.login
        const errors = await validateLogin(sMobileNo)

        if (!Object.keys(errors).length < 1) return new UserInputError('Errors', { errors })

        const user = await User.findOne({ sMobileNo, isMobileVerified: true, eStatus: { $ne: 'D' } })

        if (user && user.eStatus === 'N') {
          return new AuthenticationError('Error', { error: 'You have been blocked by administrator please contact administrator' })
        }

        if (!user) {
          return new AuthenticationError('Error', { error: 'User with this mobileNo dose not exists' })
        }
        const nOtp = genrateOtp()
        sendSMS(sMobileNo, nOtp)

        // Otp valid for 5 min
        await User.findOneAndUpdate({ sMobileNo }, { sLoginOtp: nOtp.toString(), nLoginOtpExpiry: Date.now() + 300000 })

        return { sMessage: 'Otp has been sent ' }
      } catch (error) {
        throw new InternalServerError('Error', { error: 'Something went wrong' })
      }
    },

    verifyLogin: async (parent, args, context, info) => {
      try {
        const { sMobileNo, sOtp } = args.verifylogin

        // const errors = await validateVerifyLogin(sMobileNo, sOtp)
        // if (!Object.keys(errors).length < 1) return new UserInputError('Errors', { errors })

        const user = await User.findOne({ sMobileNo, isMobileVerified: true, eStatus: 'Y' })

        if (!user) {
          return new AuthenticationError('Error', { error: 'User with this mobileNo dose not exists' })
        }

        if (!(user.nLoginOtpExpiry > Date.now())) return new GenralError('Error', { error: 'Otp Expired please sent again' })

        if (!(user.sLoginOtp === sOtp)) {
          return new AuthenticationError('Error', { error: 'Invalid Otp' })
        } else {
          const sToken = jwt.sign({ _id: user._id }, config.JWT_SECRET, { expiresIn: config.JWT_VALIDITY })

          if (user.aTokens.length === 5) {
            user.aTokens.shift()
            user.aTokens.push({ sToken })
          } else {
            user.aTokens.push({ sToken })
          }

          await User.findByIdAndUpdate(user._id, { $set: { aTokens: user.aTokens }, $unset: { sLoginOtp: '', nLoginOtpExpiry: '' } })
          return { sMessage: 'LoggedIn successfully', sToken }
        }
      } catch (error) {
        console.log(error)
        throw new InternalServerError('Error', { error: 'Something went wrong' })
      }
    }
  }
}

module.exports = resolvers
