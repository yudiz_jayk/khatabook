const mongoose = require('mongoose')

const customerSchema = new mongoose.Schema({
  sCustomerName: String,
  sMobileNo: String,

  iUserId: {
    type: mongoose.Types.ObjectId,
    ref: 'User'
  },

  iKhatabookId: {
    type: mongoose.Types.ObjectId,
    ref: 'Khatabook'
  },

  eStatus: {
    type: String,
    enum: ['Y', 'D'],
    default: 'Y'
  },

  nAmount: {
    type: Number,
    default: 0
  }

}, {
  timestamps: true
})

module.exports = mongoose.model('Customer', customerSchema)
