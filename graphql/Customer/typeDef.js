const typeDefs = `
    type Customer {
      sCustomerName: String,
      sMobileNo: String,
      iKhatabookId: ID
    }

    type responseMsgCustomer{
        sMessage:String
    }

    input createCustomer {
        sCustomerName: String!,
        sMobileNo: String
        iKhatabookId: ID!
    }

    type Query {
        getCustomer: [Customer]
    }
    
    type Mutation {
      addCustomer(customer: createCustomer): Customer!
      updateCustomer(id:ID!, customerDetail: createCustomer): responseMsgCustomer!
      deleteCustomer(id:ID!): responseMsgCustomer!
    } 
      `

module.exports = typeDefs
