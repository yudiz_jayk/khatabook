const Customer = require('./model')
const { UserInputError } = require('apollo-server-core')
const { GenralError, InternalServerError } = require('../customError')
const { validateCustomer } = require('./validator')
const ObjectId = require('mongoose').Types.ObjectId

const resolver = {
  Query: {
    getCustomer: async (parent, args, context, info) => {
      try {
        const listCustomer = await Customer.find({ eStatus: 'Y' })
        return listCustomer
      } catch (error) {
        throw new InternalServerError('Error', { error: 'Something went wrong' })
      }
    }
  },
  Mutation: {
    addCustomer: async (parent, args, context, info) => {
      try {
        const { sCustomerName, sMobileNo = undefined, iKhatabookId } = args.customer
        const errors = await validateCustomer(sCustomerName, sMobileNo, iKhatabookId)

        if (!Object.keys(errors).length < 1) return new UserInputError('Errors', { errors })

        if (!ObjectId.isValid(iKhatabookId) && iKhatabookId === '') return new GenralError('Error', { error: 'Invalid ObjectId' })

        const createCustomer = await Customer.create({ ...args.customer, iUserId: context.user._id })
        return createCustomer
      } catch (error) {
        throw new InternalServerError('Error', { error: 'Something went wrong' })
      }
    },
    updateCustomer: async (parent, args, context, info) => {
      if (!ObjectId.isValid(args.id) && args === '') return new GenralError('Error', { error: 'Invalid ObjectId' })

      const { sCustomerName, sMobileNo = undefined } = args.customerDetail

      const errors = await validateCustomer(sCustomerName, sMobileNo)

      if (!Object.keys(errors).length < 1) return new UserInputError('Errors', { errors })

      const changeCustomer = await Customer.findByIdAndUpdate(args.id, { sCustomerName }, { new: true })

      if (!changeCustomer) return new GenralError('Error', { error: 'Customer Not Found' })
      return { sMessage: 'Customer updated successfully' }
    },
    deleteCustomer: async (parent, args, context, info) => {
      try {
        const delCustomer = await Customer.findByIdAndUpdate({ _id: args.id, iUserId: context.user._id, iKhatabookId: args.iKhatabookId }, { eStatus: 'D' })
        if (!delCustomer) return new GenralError('Error', { error: 'Customer Not Found' })
        return { sMessage: `${args.id} cutomer deleted successfully` }
      } catch (error) {
        throw new InternalServerError('Error', { error: 'Something went wrong' })
      }
    }
  }
}

module.exports = resolver
