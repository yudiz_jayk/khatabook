const validator = {}

validator.validateCustomer = async (sCustomerName, sMobileNo) => {
  const errors = {}
  if (!(sCustomerName && sCustomerName.trim() && /^[a-zA-Z ]*$/.test(sCustomerName))) {
    errors.sCustomerName = 'Only alphabets allowed'
  } else if (!(sCustomerName.length >= 4)) {
    errors.sCustomerName = 'Customer Name should be gretarthen 4 characters'
  }

  if (sMobileNo && !/(0|91)[7-9][0-9]{9}/.test(sMobileNo)) {
    errors.sMobileNo = 'Invalid mobileNo format'
  }

  return errors
}

module.exports = validator
