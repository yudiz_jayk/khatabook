const Entry = require('./model')
const Customer = require('../Customer/model')
const { validateAddEntry } = require('./validator')
const { UserInputError } = require('apollo-server-core')
const { GenralError, InternalServerError } = require('../customError')
const ObjectId = require('mongoose').Types.ObjectId
const { calculateNetAmount } = require('../../utils/utilites')

const resolvers = {

  Query: {
    getEntries: async (parent, args, context, info) => {
      try {
        const entries = await Entry.find(context.user._id)
        return entries
      } catch (error) {
        throw new InternalServerError('Error', { error: 'Something went wrong' })
      }
    },

    getEntryById: async (parent, args, context, info) => {
      try {
        const entry = await Entry.findById(args.id).where('iUserId').equals(context.user._id)
        if (!entry) return GenralError('Error', { error: 'Entry with this object id not found' })
        return entry
      } catch (error) {
        throw new InternalServerError('Error', { error: 'Something went wrong' })
      }
    }

  },

  Mutation: {
    createEntry: async (parent, args, context, info) => {
      try {
        const { iCustomerId, sDescription, paidByUser } = args.entry
        const iUserId = context.user._id
        args.iUserId = iUserId

        const { nAmount } = args.entry

        const errors = validateAddEntry(nAmount, sDescription, paidByUser)
        if (!Object.keys(errors).length < 1) return new UserInputError('Errors', { errors })

        if (!ObjectId.isValid(iCustomerId)) {
          return GenralError('Error', { error: 'Invalid CustomerID' })
        }

        if (!ObjectId.isValid(iUserId)) {
          return GenralError('Error', { error: 'Invalid UserId' })
        }

        const entry = await Entry.create({ ...args.entry })

        await Customer.findByIdAndUpdate(iCustomerId, { $inc: { nAmount } })

        return entry
      } catch (error) {
        throw new InternalServerError('Error', { error: 'Something went wrong' })
      }
    },
    updateEntry: async (parent, args, context, info) => {
      try {
        const { id, iCustomerId, nAmount, sDescription, paidByUser } = args.entry
        const iUserId = context.user._id
        args.iUserId = iUserId

        const errors = validateAddEntry(nAmount, sDescription, paidByUser)
        if (!Object.keys(errors).length < 1) return new UserInputError('Errors', { errors })

        if (!ObjectId.isValid(id)) {
          return GenralError('Error', { error: 'Invalid EntryId' })
        }

        const entry = await Entry.findById(id)
        if (!entry) return GenralError('Error', { error: 'Entry with this object id not found' })

        if (!ObjectId.isValid(iCustomerId)) {
          return GenralError('Error', { error: 'Invalid CustomerID' })
        }

        if (!ObjectId.isValid(iUserId)) {
          return GenralError('Error', { error: 'Invalid UserId' })
        }

        const updatedEntry = await Entry.findByIdAndUpdate(entry._id, args.entry)

        const nNetAmount = await calculateNetAmount(iUserId, iCustomerId)

        await Customer.findByIdAndUpdate(iCustomerId, { nAmount: nNetAmount })

        return updatedEntry
      } catch (error) {
        console.log(error)
        throw new InternalServerError('Error', { error: 'Something went wrong' })
      }
    },
    deleteEntry: async (parent, args, context, info) => {
      try {
        const entry = await Entry.findByIdAndUpdate(args.id, { eStatus: 'D' }).where('iUserId').equals(context.user._id)
        if (!entry) return new GenralError('Error', { error: 'Entry with this object id not found' })

        const nNetAmount = await calculateNetAmount(entry.iUserId, entry.iCustomerId)

        await Customer.findByIdAndUpdate(entry.iCustomerId, { nAmount: nNetAmount })

        return { sMessage: 'Entry deleted successfully' }
      } catch (error) {
        throw new InternalServerError('Error', { error: 'Something went wrong' })
      }
    }

  }

}

module.exports = resolvers
