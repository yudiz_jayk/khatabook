const mongoose = require('mongoose')

const entrySchema = new mongoose.Schema({
  iCustomerId: {
    type: mongoose.Types.ObjectId,
    ref: 'Customer'
  },

  iUserId: {
    type: mongoose.Types.ObjectId,
    ref: 'User'
  },

  nAmount: {
    type: Number
  },

  sDescription: {
    type: String
  },

  iKhataBookId: {
    type: mongoose.Types.ObjectId,
    ref: 'Khatabook'
  },

  paidByUser: Boolean,

  eStatus: {
    type: String,
    enum: ['Y', 'D'],
    default: 'Y'
  }

}, {
  timestamps: true
})

module.exports = mongoose.model('Entry', entrySchema)
