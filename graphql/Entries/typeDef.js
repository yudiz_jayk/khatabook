const typeDef = `
     type responseMsgEntry{
        sMessage:String
     }
   
     type Entry{
        iCustomerId:String!,
        nAmount:Int!,
        sDescription:String,
        paidByUser:Boolean!
        iKhataBookId:String!
     }

     input entryInput{
        iCustomerId:String!,
        nAmount:Int!,
        sDescription:String,
        paidByUser:Boolean!
        iKhataBookId:String!
     }

     input updateEntryInput{
      id:ID!
      iCustomerId:String!,
      nAmount:Int!,
      sDescription:String,
      paidByUser:Boolean!
   }

     type Mutation{
        createEntry(entry:entryInput):Entry!
        updateEntry(entry:updateEntryInput):Entry!
        deleteEntry(id:ID!):responseMsgEntry!
     }

     type Query{
      getEntries:[Entry]!
      getEntryById(id:ID!):Entry!
     }
`

module.exports = typeDef
