const validator = {}

validator.validateAddEntry = (nAmount, sDescription, paidByUser) => {
  const errors = {}

  if (nAmount <= 0) {
    errors.nAmount = 'Amount should be greater then 0'
  }

  if (sDescription.length < 10 && sDescription.length > 100) {
    errors.sDescription = 'Description should be between to 10-100 characters'
  }

  if (typeof paidByUser !== 'boolean') {
    errors.paidByUser = 'Invalid Field'
  }

  return errors
}

module.exports = validator
