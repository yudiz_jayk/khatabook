const DataLoader = require('dataloader')
const Khatabook = require('../Khatabook/model')
const Customer = require('../Customer/model')

module.exports.khatabookByuserid = () => {
  return new DataLoader(kBookbyuid)
}

module.exports.customerBykhatabookid = () => {
  return new DataLoader(customerBykid)
}

async function kBookbyuid (userIds) {
  console.log('Query fired at timesds')
  const kahatabooks = await Khatabook.find({ iUserId: { $in: userIds } })
  const groupByUser = kahatabooks.reduce((acc, curr) => {
    acc[curr.iUserId] = acc[curr.iUserId] ?? []
    acc[curr.iUserId].push(curr)
    return acc
  }, {})

  return userIds.map((cid) => groupByUser[cid])
}

async function customerBykid (khataBookids) {
  console.log('Query fired at timesds')
  const customers = await Customer.find({ iKhatabookId: { $in: khataBookids } })
  const groupBykid = customers.reduce((acc, curr) => {
    acc[curr.iKhatabookId] = acc[curr.iKhatabookId] ?? []
    acc[curr.iKhatabookId].push(curr)
    return acc
  }, {})

  return khataBookids.map((cid) => groupBykid[cid])
}

// module.exports.customerByKhatabook = function customerDataLoader () {
//   return new DataLoader(customerByKhatabook, {
//     batchScheduleFn: callback => setTimeout(callback, 10000)
//   })
// }

// async function customerByKhatabook (customerIds) {
//   const customers = await Customer.find({ iKhatabookId: { $in: customerIds } })

//   console.log('ff', customerIds)

//   const groupByKhatabook = customers.reduce((acc, curr) => {
//     acc[curr.iKhatabookId] = acc[curr.iKhatabookId] ?? []
//     acc[curr.iKhatabookId].push(curr)
//     return acc
//   }, {})

//   return customerIds.map((cid) => groupByKhatabook[cid])
// }
