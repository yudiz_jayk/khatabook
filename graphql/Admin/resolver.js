const Admin = require('./model')
const { validateLogin } = require('./validator')
const { UserInputError } = require('apollo-server-core')
const { GenralError, InternalServerError } = require('../customError')
const jwt = require('jsonwebtoken')
const config = require('../../config')
const User = require('../User/model')

const resolvers = {
  Query: {
    getDetails: async (parent, args, context, info) => {
      const customers = await User.find({}, { sName: true, sMobileNo: true }).lean()
      return customers
    }

  },
  users: {
    aKhatabooks: async (parent, args, context, info) => {
      const { loaders } = context
      const { khatabookbyUid } = loaders

      const khatabooks = await khatabookbyUid.load(parent._id)
      return khatabooks
    }
  },
  khatabook: {
    aCustomers: async (parent, args, context, info) => {
      const { loaders } = context
      const { customersbyKid } = loaders

      const customers = await customersbyKid.load(parent._id)
      return customers
    }
  },

  Mutation: {
    adminLogin: async (parent, args, context, info) => {
      try {
        const { sUsername, sPassword } = args.login

        const errors = await validateLogin(sUsername, sPassword)
        if (!Object.keys(errors).length < 1) return new UserInputError('Errors', { errors })

        const admin = await Admin.findOne({ sUsername, sPassword })

        if (!admin) return new GenralError('Error', { error: 'Invalid credentials' })

        const sToken = jwt.sign({ _id: admin._id }, config.JWT_SECRET, { expiresIn: config.JWT_VALIDITY })

        return sToken
      } catch (error) {
        console.log(error)
        throw new InternalServerError('Error', { error: 'Something went wrong' })
      }
    }
  }
}

module.exports = resolvers
