const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')
const config = require('../../config')

const adminSchema = new mongoose.Schema({
  sUsername: String,
  sPassword: String
}, {
  timestamps: true
})

adminSchema.statics.findByToken = function (token) {
  const admin = this
  let decoded
  try {
    decoded = jwt.verify(token, config.JWT_SECRET)
  } catch (e) {
    return Promise.reject(e)
  }
  const query = {
    _id: decoded._id
  }

  return admin.findOne(query)
}

module.exports = mongoose.model('Admin', adminSchema)
