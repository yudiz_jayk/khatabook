const typedef = `
 type khatabook{
   sName:String
   sBusinessName:String
   sBusinessDetail:String
   aCustomers:[customer]
  }

  type customer{
      sName:String
      sMobileNo:String
      nAmount:Int
  }

  type users{
    sName:String
    sMobileNo:String
    aKhatabooks:[khatabook]
  }   

   input Login{
     sUsername:String
     sPassword:String
   }

   type Query{
     getDetails:[users]
   }
 
  type Mutation {
     adminLogin(login:Login):String
  }
`
module.exports = typedef
