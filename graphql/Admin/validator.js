
const validator = {}

validator.validateLogin = async (sUserName, sPassword) => {
  const errors = {}
  if (!sUserName || sUserName.trim() === 0) {
    errors.sUserName = 'Username required'
  }

  if (!sPassword || sUserName.trim() === 0) {
    errors.sPassword = 'Password Required'
  }
  return errors
}

module.exports = validator
